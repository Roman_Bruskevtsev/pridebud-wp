'use strict';
var themeName   = 'pridebud',
    gulp        = require('gulp'),
    watch       = require('gulp-watch'),
    sass        = require('gulp-sass'),
    cssmin      = require('gulp-cssmin'),
    babel       = require('gulp-babel'),
    uglify      = require('gulp-uglify-es').default,
    rename      = require('gulp-rename'),
    concat      = require('gulp-concat'),
    pipeline    = require('readable-stream').pipeline,
    buildFolder = themeName,
    scriptsArray= [
        buildFolder + '/assets/js/slick.js',
        buildFolder + '/assets/js/mousewheel-smoothscroll.js',
        buildFolder + '/assets/js/lightgallery.js',
        buildFolder + '/assets/js/aos.js',
        buildFolder + '/assets/js/select2.full.js',
        buildFolder + '/assets/js/main.js'
    ];

gulp.task('styles',  function () {
    return gulp.src(buildFolder + '/assets/sass/**/*.scss')
        .pipe(sass.sync().on('error', sass.logError))
        .pipe(gulp.dest(buildFolder + '/assets/css'))
        .pipe(cssmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(buildFolder + '/assets/css/'));    
});

gulp.task('scripts', function () {
    return gulp.src(scriptsArray).pipe(concat('all.js'))
            .pipe(rename({suffix: '.min'}))
            .pipe(uglify())
            .pipe(gulp.dest(buildFolder + '/assets/js/'));
});

gulp.task('theme', function() {
    return gulp.src(buildFolder + '/**/*')
            .pipe(gulp.dest(buildFolder));
});

gulp.task('default', gulp.series(
    gulp.parallel(
    'styles',
    'scripts'
    )
));

gulp.task('watch', function () {
    gulp.watch(buildFolder + '/assets/sass/**/*.scss', gulp.series('styles'));

    gulp.watch(scriptsArray, gulp.series('scripts'));
});