<?php
/**
 *
 * @package WordPress
 * @subpackage Pridebud
 * @since 1.0
 */

/*Trainers post type*/
require get_template_directory() . '/inc/post-type-function.php';

/*Translation*/
require get_template_directory() . '/inc/translation.php';

/*Theme setup*/
function pridebud_setup() {
    load_theme_textdomain( 'pridebud' );
    add_theme_support( 'automatic-feed-links' );
    add_theme_support( 'title-tag' );
    add_theme_support( 'post-thumbnails' );

    add_image_size( 'post-small', 430, 400, true );
    add_image_size( 'portfolio-thumbnail', 1300, 660, true );
    add_image_size( 'portfolio-medium', 825, 464, true );

    register_nav_menus( array(
        'main'          => __( 'Main Menu', 'pridebud' ),
        'services'      => __( 'Services Menu', 'pridebud' ),
    ) );
}
add_action( 'after_setup_theme', 'pridebud_setup' );

/*Styles and scripts*/
function pridebud_scripts() {
    $version = '1.0.25';

    wp_enqueue_style( 'pridebud-css', get_theme_file_uri( '/assets/css/main.min.css' ), '', $version );
    wp_enqueue_style( 'swiper-css', 'https://unpkg.com/swiper/swiper-bundle.min.css', '', $version );
    wp_enqueue_style( 'pridebud-style', get_stylesheet_uri() );

    wp_enqueue_script( 'swiper-js', 'https://unpkg.com/swiper/swiper-bundle.min.js', array( 'jquery' ), $version, true );
    wp_enqueue_script( 'all-js', get_theme_file_uri( '/assets/js/all.min.js' ), array( 'jquery' ), $version, true );
}
add_action( 'wp_enqueue_scripts', 'pridebud_scripts' );

/*Theme settings*/
if( function_exists('acf_add_options_page') ) {

    $general = acf_add_options_page(array(
        'page_title'    => 'Theme General Settings',
        'menu_title'    => 'Theme Settings',
        'redirect'      => false,
        'capability'    => 'edit_posts',
        'menu_slug'     => 'theme-settings',
    ));
    
    // acf_add_options_sub_page(array(
    //     'page_title'    => 'Main Page Settings',
    //     'menu_title'    => 'Main Page',
    //     'parent_slug'   => $general['menu_slug']
    // ));
}


/*SVG support*/
function pridebud_svg_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'pridebud_svg_types');

/*App2drive ajax*/
function pridebud_ajaxurl() {
?>
    <script type="text/javascript">
        var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
        <?php if( get_field( 'api_key', 'option' ) ) { ?>
            var apiKey = '<?php the_field( 'api_key', 'option' ); ?>';
        <?php } ?>
    </script>
<?php
}
add_action('wp_footer','pridebud_ajaxurl');