<?php
/**
 *
 * @package WordPress
 * @subpackage Pridebud
 * @since 1.0
 * @version 1.0
 */
?>  
    </main>
    <footer> 
        <div class="contact__line d-block d-lg-none">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xl-6">
                        <div class="address__info float-left">
                            <?php if( get_field('address', 'option') ) { ?>
                                <a target="_blank" href="https://www.google.com.ua/maps/search/<?php the_field('address', 'option'); ?>" class="address float-left"><?php the_field('address', 'option'); ?></a>
                            <?php } 
                            if( get_field('timetable', 'option') ) { ?>
                                <div class="timetable float-left"><?php the_field('timetable', 'option'); ?></div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="col-xl-6">
                        <div class="contacts__info float-left">
                            <?php if( get_field('email', 'option') ) { ?>
                            <div class="email__block float-left">
                                <a href="mailto:<?php the_field('email', 'option'); ?>"><?php the_field('email', 'option'); ?></a>
                            </div>
                            <?php } ?>
                            <?php if( get_field('phones_numbers', 'option') ) { ?>
                            <div class="phone__block float-left">
                                <?php foreach ( get_field('phones_numbers', 'option') as $phone ) { ?>
                                <a href="tel:<?php echo $phone['phone']; ?>"><?php echo $phone['phone']; ?></a>
                                <?php } ?>
                            </div>
                            <?php } ?> 
                            <div class="social__links float-right">
                                <?php if( get_field('facebook', 'option') ) { ?><a href="<?php the_field('facebook', 'option'); ?>" target="_blank" class="facebook"></a><?php } ?> 
                                <?php if( get_field('instagram', 'option') ) { ?><a href="<?php the_field('instagram', 'option'); ?>" target="_blank" class="instagram"></a><?php } ?> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="calback__button callback">
            <div class="tip__block"><?php the_field('callback_button_tip', 'option'); ?></div>
        </div>
    </footer>
    <div class="popup__wrapper"></div>
    <div id="call__back" class="popup__sidebar hide__sidebar">
        <div class="popup_block">
            <?php if(get_field('sidebar_title', 'option')); { ?><h2><?php the_field('sidebar_title', 'option'); ?></h2><?php } 
            echo do_shortcode( get_field('sidebar_shortcode', 'option') );
            ?>
        </div>
    </div>
    <div id="get__price" class="popup__sidebar hide__sidebar">
        <div class="popup_block">
            <?php if(get_field('sidebar_title_get_price', 'option')); { ?><h2><?php the_field('sidebar_title_get_price', 'option'); ?></h2><?php } 
            echo do_shortcode( get_field('sidebar_shortcode_get_price', 'option') );
            ?>
        </div>
    </div>
    <!-- <div class="move__circle"></div> -->
    <?php wp_footer(); ?>
    <?php if( ENV == 'prd' ) { ?>
    <!-- BEGIN PLERDY CODE -->
    <script type="text/javascript" defer>
        var _protocol = (("https:" == document.location.protocol) ? " https://" : " http://");
        var _site_hash_code = "bfd591b649ec25d2ea6088fd920c2df4";
        var _suid = 12923;
        </script>
    <script type="text/javascript" defer src="https://a.plerdy.com/public/js/click/main.js"></script>
    <!-- END PLERDY CODE -->
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N7NJHV9"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <?php } ?> 
</body>
</html>