<?php
/**
 *
 * @package WordPress
 * @subpackage Pridebud
 * @since 1.0
 * @version 1.0
 */
get_header(); 
$background = (get_field('background_image', 'option')) ? 'style="background-image: url('.get_field('background_image', 'option').');"' : '';
$width = (get_field('width')) ? ' full' : '';
?>

    <div class="page__wrapper overflow-x-hidden"<?php echo $background; ?>>
        <div class="content__wrapper blog">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <h1><?php the_title(); ?></h1>
                        <?php if ( have_posts() ) :
                            echo '<div class="blog__section">';
                            $i = 1;
                            while ( have_posts() ) : the_post();
                                if ( $i % 2 != 0) {
                                    get_template_part( 'template-parts/post/content', 'left' );
                                } else {
                                    get_template_part( 'template-parts/post/content', 'right' );
                                }
                            $i++;
                            endwhile;
                            echo '</div>';
                        endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>


<?php get_footer();