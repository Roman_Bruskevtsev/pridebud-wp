<?php
add_action('init', 'pridebud_objects_post', 0);

function pridebud_objects_post() {
  //Register taxonomy
  $taxonomy_labels = array(
    'name'                        => __('Objects categories','pridebud'),
    'singular_name'               => __('Object category','pridebud'),
    'menu_name'                   => __('Objects categories','pridebud'),
  );

  $taxonomy_rewrite = array(
    'slug'                  => 'objects-categories',
    'with_front'            => true,
    'hierarchical'          => true,
  );

  $taxonomy_args = array(
    'labels'              => $taxonomy_labels,
    'hierarchical'        => true,
    'public'              => true,
    'show_ui'             => true,
    'show_admin_column'   => true,
    'show_in_nav_menus'   => true,
    'show_tagcloud'       => true,
    'rewrite'             => $taxonomy_rewrite,
  );
  register_taxonomy( 'objects-categories', 'objects', $taxonomy_args );
  
  //Register new post type
  $projects_labels = array(
  'name'                => __('Objects', 'pridebud'),
  'add_new'             => __('Add Object', 'pridebud')
  );

  $projects_args = array(
  'label'               => __('Objects', 'pridebud'),
  'description'         => __('Object information page', 'pridebud'),
  'labels'              => $projects_labels,
  'supports'            => array( 'title', 'thumbnail'),
  'taxonomies'          => array( 'objects-categories' ),
  'hierarchical'        => false,
  'public'              => true,
  'show_ui'             => true,
  'show_in_menu'        => true,
  'has_archive'         => true,
  'can_export'          => true,
  'show_in_nav_menus'   => true,
  'publicly_queryable'  => true,
  'exclude_from_search' => false,
  'query_var'           => true,
  'rewrite'             => '',
  'capability_type'     => 'post',
  'menu_icon'           => 'dashicons-building'
  );
  register_post_type( 'objects', $projects_args );

  $projects_labels = array(
  'name'                => __('Services', 'pridebud'),
  'add_new'             => __('Add Service', 'pridebud')
  );

  $projects_args = array(
  'label'               => __('Services', 'pridebud'),
  'description'         => __('Service information page', 'pridebud'),
  'labels'              => $projects_labels,
  'supports'            => array( 'title', 'thumbnail'),
  'hierarchical'        => false,
  'public'              => true,
  'show_ui'             => true,
  'show_in_menu'        => true,
  'has_archive'         => true,
  'can_export'          => true,
  'show_in_nav_menus'   => true,
  'publicly_queryable'  => true,
  'exclude_from_search' => false,
  'query_var'           => true,
  'rewrite'             => '',
  'capability_type'     => 'post',
  'menu_icon'           => 'dashicons-admin-tools'
  );
  register_post_type( 'services', $projects_args );
}
