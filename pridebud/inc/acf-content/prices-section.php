<div class="prices__section">
	<div class="container">
        <div class="row">
            <div class="col">
        		<div class="text" data-aos-duration="500" data-aos="fade-up">
        			<?php the_sub_field('text'); ?>
        			<?php if(get_sub_field('title')) { ?><h3><?php the_sub_field('title'); ?></h3><?php } ?>
        		</div>
            </div>
        </div>
        <?php 
        $blocks = get_sub_field('prices_blocks');
        if( $blocks ) { ?>
        <div class="row">
        	<?php foreach ( $blocks as $block ) { ?>
        		<div class="col-md-6 col-lg-4">
        			<div class="price__block" data-aos-duration="500" data-aos="fade-up">
        				<?php if( $block['title'] ) { ?>
        				<div class="title text-center">
        					<h5><?php echo $block['title']; ?></h5>
        				</div>
        				<?php } 
        				if( $block['parameters'] ) { ?>
        				<div class="content">
                            <?php echo $block['parameters']; ?>
                            <div class="more__link text-center"><?php echo SHOWMORE; ?></div>
                        </div>
        				<?php } 
        				if( $block['price'] ) { ?>
        				<div class="price"><?php echo $block['price']; ?></div>
        				<?php } ?>
        			</div>
        		</div>
        	<?php } ?>
        </div>
        <?php } ?>
    </div>
</div>