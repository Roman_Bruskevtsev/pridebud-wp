<div class="company__benefits">
	<div class="container">
		<div class="row">
			<div class="col">
				<?php if(get_sub_field('title')) { ?>
					<h2 data-aos-duration="500" data-aos="fade-up"><?php the_sub_field('title'); ?></h2>
				<?php } 
				$sections = get_sub_field('benefits_section'); 
				if( $sections ) { ?>
					<div class="benefits__sections">
						<div class="row">
							<div class="col-lg-5">
								<div class="benefit__nav__bar">
								<?php 
								$i = 1; 
								foreach ( $sections as $section ) { ?>
									<h3<?php echo $i == 1 ? ' class="active"' : ''; ?>><?php echo $section['title']; ?></h3>
								<?php $i++; } ?>
								</div>
							</div>
							<div class="col-lg-7">
							<?php 
							$i = 0;
							foreach ( $sections as $section ) { ?>
								<div class="benefit__section">
									<div class="content">
										<?php 
										$benefits = $section['benefits'];
										foreach ( $benefits as $benefit ) { ?>
											<div class="benefit">
												<?php if( $benefit['icon'] ) { ?>
													<div class="icon">
														<img src="<?php echo $benefit['icon']['url']; ?>" alt="<?php echo $benefit['icon']['title']; ?>">
													</div>
												<?php }	
												if( $benefit['title'] ) { ?>
													<h4><?php echo $benefit['title']; ?></h4>
												<?php }	
												echo $benefit['text']; ?>
											</div>
										<?php }	?>
									</div>
								</div>
							<?php $i++; } ?>
							</div>
						</div>
					</div>
			    <?php } ?>
			</div>
		</div>
	</div>
</div>