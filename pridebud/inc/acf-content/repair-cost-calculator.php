<?php 
$step_1 = get_sub_field('step_1');
$step_2 = get_sub_field('step_2');
$step_3 = get_sub_field('step_3');
$step_4 = get_sub_field('step_4');
$step_5 = get_sub_field('step_5');
$objects_type = $step_1['objects_type'];
$repair_type = $step_4['repair_type'];
$form_shortcode = get_sub_field('contact_form_7_shortcode');
?>
<div class="repair__cost__calculator__section">
	<div class="container">
		<div class="row">
			<div class="col">
				<div class="repair__cost__calculator__bullets">
					<div class="bullet active">
					<?php if( $step_1['bullet_title'] ) { ?>
						<h6><?php echo $step_1['bullet_title']; ?></h6>
					<?php } ?>
					</div>
					<div class="bullet">
					<?php if( $step_2['bullet_title'] ) { ?>
						<h6><?php echo $step_2['bullet_title']; ?></h6>
					<?php } ?>
					</div>
					<div class="bullet">
					<?php if( $step_3['bullet_title'] ) { ?>
						<h6><?php echo $step_3['bullet_title']; ?></h6>
					<?php } ?>
					</div>
					<div class="bullet">
					<?php if( $step_4['bullet_title'] ) { ?>
						<h6><?php echo $step_4['bullet_title']; ?></h6>
					<?php } ?>
					</div>
					<div class="bullet">
					<?php if( $step_5['bullet_title'] ) { ?>
						<h6><?php echo $step_5['bullet_title']; ?></h6>
					<?php } ?>
					</div>
				</div>
				<div class="repair__cost__calculator__steps">
					<div class="repair__cost__calculator__step active">
						<?php if( $step_1['title'] ) { ?>
						<div class="repair__cost__step__title">
							<h3><?php echo $step_1['title']; ?></h3>
						</div>
						<?php } 
						if( $objects_type ) { ?>
						<div class="row">
							<?php foreach ($objects_type as $type ) { ?>
							<div class="col-md-6 col-lg-4">
								<label class="radio__field__group">
									<input class="radio__field repair__field" type="radio" name="object-type" value="<?php echo $type['title']; ?>" required>
									<div class="radio__field__block">
										<div class="thumbnail" style="background-image: url(<?php echo $type['image']; ?>);"></div>
										<div class="title">
											<h6><?php echo $type['title']; ?></h6>
										</div>
									</div>
								</label>
							</div>
							<?php } ?>
						</div>
						<?php } ?>
						<div class="steps__navigation">
							<div class="validity__field"></div>
							<button class="btn yellow__btn small next-step" type="button"><span><?php echo NEXT; ?></span></button>
						</div>
					</div>
					<div class="repair__cost__calculator__step">
						<?php if( $step_2['title'] ) { ?>
						<div class="repair__cost__step__title">
							<div class="repair__cost__step__title">
								<h3><?php echo $step_2['title']; ?></h3>
							</div>
						</div>
						<?php } ?>
						<div class="row">
								<div class="col-lg-5">
									<label class="field__group default">
										<input type="number" class="form__field no__shadow repair__field" name="perimeter-area" min="1" step="0.1" required>
									</label>
								</div>
							</div>
						<div class="steps__navigation">
							<button class="btn yellow__btn small prev-step" type="button"><span><?php echo BACK; ?></span></button>
							<button class="btn yellow__btn small next-step" type="button"><span><?php echo NEXT; ?></span></button>
						</div>
					</div>
					<div class="repair__cost__calculator__step">
						<?php if( $step_3['title'] ) { ?>
						<div class="repair__cost__step__title">
							<h3><?php echo $step_3['title']; ?></h3>
						</div>
						<?php } ?>
						<div class="row">
							<div class="col-lg-5">
								<label class="field__group default">
									<input type="number" class="form__field no__shadow repair__field" name="number-rooms" min="1" step="1" required>
								</label>
							</div>
						</div>
						<div class="steps__navigation">
							<button class="btn yellow__btn small prev-step" type="button"><span><?php echo BACK; ?></span></button>
							<button class="btn yellow__btn small next-step" type="button"><span><?php echo NEXT; ?></span></button>
						</div>
					</div>
					<div class="repair__cost__calculator__step">
						<?php if( $step_4['title'] ) { ?>
						<div class="repair__cost__step__title">
							<h3><?php echo $step_4['title']; ?></h3>
						</div>
						<?php } 
						if( $repair_type ) { ?>
						<div class="row">
							<?php foreach ($repair_type as $type ) { ?>
							<div class="col-md-6 col-lg-4">
								<label class="radio__field__group">
									<input class="radio__field repair__field" type="radio" name="repair-type" value="<?php echo $type['title']; ?>" required>
									<div class="radio__field__block">
										<div class="thumbnail" style="background-image: url(<?php echo $type['image']; ?>);"></div>
										<div class="title">
											<h6><?php echo $type['title']; ?></h6>
										</div>
									</div>
								</label>
							</div>
							<?php } ?>
						</div>
						<?php } ?>
						<div class="steps__navigation">
							<button class="btn yellow__btn small prev-step" type="button"><span><?php echo BACK; ?></span></button>
							<button class="btn yellow__btn small next-step" type="button"><span><?php echo NEXT; ?></span></button>
						</div>
					</div>
					<div class="repair__cost__calculator__step">
						<?php if( $step_5['title'] ) { ?>
						<div class="repair__cost__step__title">
							<h3><?php echo $step_5['title']; ?></h3>
						</div>
						<?php } ?>
						<div class="row">
							<div class="col-lg-5">
								<div class="repair__cost__summary">
									<div class="form__text__field object__type">
										<h6><?php echo OBJECTTYPETEXT; ?></h6>
										<div class="value"></div>
									</div>
									<div class="form__text__field perimeter__area">
										<h6><?php echo PERIMETERAREA; ?></h6>
										<div class="value"></div>
									</div>
									<div class="form__text__field number__rooms">
										<h6><?php echo NUMBEROFROOMS; ?></h6>
										<div class="value"></div>
									</div>
									<div class="form__text__field repair__type">
										<h6><?php echo REPAIRTYPETEXT; ?></h6>
										<div class="value"></div>
									</div>
								</div>
							</div>
							<div class="col-lg-7">
								<?php if( $step_5['form_text'] ) { ?>
								<div class="form__text">
									<?php echo $step_5['form_text']; ?>
								</div>
								<?php } 
								if( $form_shortcode ) { ?>
								<div class="form__block">
									<?php echo do_shortcode($form_shortcode); ?>
								</div>
								<?php } ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>