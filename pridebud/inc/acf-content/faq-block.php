<?php if( get_field('get_price_button_label', 'option') ) { ?>
<div class="btn__group small-margin">
    <div class="container">
        <div class="row">
            <div class="col">
                <button type="button" class="btn yellow__btn"><span><?php the_field('get_price_button_label', 'option'); ?></span></button>
            </div>
        </div>
    </div>
</div>
<?php } ?>
<div class="faq__block">
	<div class="container">
		<div class="row">
			<div class="col">
				<?php if( get_sub_field('title') ) { ?>
					<h3 data-aos-duration="500" data-aos="fade-up"><?php the_sub_field('title'); ?></h3>
				<?php } 
				$faqs = get_sub_field('faq'); 
				if( $faqs ) { ?>
				<div class="faq__wrapper">
					<?php foreach ( $faqs as $faq ) { ?>
						<div class="faq__row" data-aos-duration="500" data-aos="fade-up">
							<div class="icon"></div>
							<?php if( $faq['title'] ) { ?><div class="title"><h6><?php echo $faq['title']; ?></h6></div><?php } ?>
							<?php if( $faq['text'] ) { ?><div class="content"><?php echo $faq['text']; ?></div><?php } ?>
						</div>
					<?php } ?>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>