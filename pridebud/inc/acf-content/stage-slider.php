<div class="stage__slider__block" data-aos-duration="500" data-aos="fade-up">
	<?php if(get_sub_field('title')) { ?><h3><?php the_sub_field('title'); ?></h3><?php }
	$slider = get_sub_field('slider');
	if( $slider ) { ?>
	<div class="stage__slider__wrapper">
		<div class="stage__slider__navigation">
			<?php 
			$i = 1;
			foreach ( $slider as $slide ) { ?>
				<div class="slide">
					<div class="nav__block">
						<?php if( $slide['stage_name'] ) { ?>
						<div class="stage">
							<span><?php echo $i; ?></span>
							<h6><?php echo $slide['stage_name']; ?></h6>
							<h6 class="title"><?php echo $slide['title']; ?></h6>
						</div>
						<?php } ?>
						<div class="content">
							<?php if( $slide['title'] ) { ?><h4><?php echo $slide['title']; ?></h4><?php } 
							echo $slide['text']; ?>
						</div>
					</div>
				</div>
			<?php 
			$i++;
			} ?>
		</div>
		<div class="stage__slider__image">
			<?php foreach ( $slider as $slide ) { ?>
				<div class="slide">
					<?php if( $slide['image'] ) { ?>
					<div class="image" style="background-image: url(<?php echo $slide['image']; ?>)"></div>
					<?php } ?>
				</div>
			<?php } ?>	
		</div>
	</div>
	<?php } ?>
</div>