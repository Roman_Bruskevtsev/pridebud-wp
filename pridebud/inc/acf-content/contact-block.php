<div class="container">
    <div class="row">
        <div class="col-md-6">
            <div class="contact__block">
                <div class="contacts">
                    <?php if(get_sub_field('contact_title')) { ?><h3><?php the_sub_field('contact_title'); ?></h3><?php } ?>
                    <?php if(get_sub_field('address')) { ?>
                        <a href="https://www.google.com.ua/maps/search/<?php the_sub_field('address'); ?>" target="_blank" class="address__line"><?php the_sub_field('address'); ?></a>
                    <?php } ?>
                    <?php if(get_sub_field('phones')) { ?>
                    <div class="phone__block">
                        <?php foreach ( get_sub_field('phones') as $phone ) { ?>
                        <div>
                            <a href="tel:<?php echo $phone['phone_number']; ?>"><?php echo $phone['phone_number']; ?></a>
                            <span><?php echo $phone['operator']; ?></span>
                        </div>
                        <?php } ?>
                    </div>
                    <?php } ?>
                    <?php if(get_sub_field('email')) { ?>
                    <div class="email__block">
                        <a href="mailto:<?php the_sub_field('email'); ?>"><?php the_sub_field('email'); ?></a>
                    </div>
                    <?php } ?>
                    <?php if(get_sub_field('timetable')) { ?>
                    <div class="timetable__block">
                        <?php the_sub_field('timetable'); ?>
                    </div>
                    <?php } ?>
                </div>
                <div class="social">
                    <?php if(get_sub_field('social_title')) { ?><h3><?php the_sub_field('social_title'); ?></h3><?php } ?>
                    <div class="social__links">
                        <?php if(get_sub_field('facebook')) { ?><a target="_blank" href="<?php the_sub_field('facebook'); ?>" class="facebook"></a><?php } ?>
                        <?php if(get_sub_field('instagram')) { ?><a target="_blank" href="<?php the_sub_field('instagram'); ?>" class="instagram"></a><?php } ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="contact__form full">
                <?php if(get_sub_field('form_title')) { ?><h2><?php the_sub_field('contact_title'); ?></h2><?php } ?>
                <?php echo do_shortcode( get_sub_field('form_shortcode') ); ?>
            </div>
        </div>
    </div>
</div>