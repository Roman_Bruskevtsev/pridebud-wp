<?php 
$images = get_sub_field('images');

if($images) { ?>
<div class="image__block">
    <div class="container">
        <div class="row">
            <div class="col">
                <?php foreach( $images as $image ): ?>
                <a class="image" href="<?php echo $image['url']; ?>" class="image">
                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"/>
                </a>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</div>
<?php } ?>