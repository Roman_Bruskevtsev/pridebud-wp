<div class="contact__form">
	<div class="container">
        <div class="row">
            <div class="col">
			    <?php if(get_sub_field('title')) { ?><h2><?php the_sub_field('title'); ?></h2><?php } ?>
			    <div class="form__inline">
			        <?php echo do_shortcode( get_sub_field('form_shortcode') ); ?>
			    </div>
			</div>
		</div>
	</div>
</div>