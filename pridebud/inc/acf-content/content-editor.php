<div class="text__block" data-aos-duration="500" data-aos="fade-up">
	<div class="container">
        <div class="row">
            <div class="col">
    			<?php the_sub_field('text'); ?>
    		</div>
    	</div>
    </div>
</div>