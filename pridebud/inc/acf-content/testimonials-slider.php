<div class="testimonials__block">
    <div class="container">
        <div class="row">
            <div class="col">
                <?php if(get_sub_field('title')) { ?><h2><?php the_sub_field('title'); ?></h2><?php } ?>
                <?php if( have_rows('testimonials') ): ?>
                <div class="testimonials__slider">
                    <?php while ( have_rows('testimonials') ) : the_row(); ?>
                    <div class="slide">
                        <div class="block">
                            <?php if( get_sub_field('text') ) { ?><p><?php the_sub_field('text'); ?></p><?php } ?>
                            <?php if( get_sub_field('avatar') ) { 
                                $background = get_sub_field('avatar'); ?>
                                <div class="avatar" style="background-image: url('<?php echo $background; ?>');"></div>
                            <?php }
                            if( get_sub_field('name') ) { ?>
                                <h6><?php the_sub_field('name'); ?></h6>
                            <?php } ?>
                        </div>
                    </div>
                    <?php endwhile; ?>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>