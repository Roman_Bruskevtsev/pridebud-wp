<div class="text-image-section-white">
	<div class="container">
		<div class="row">
			<?php if( get_sub_field('text') ) { ?>
			<div class="col-lg-6">
				<div class="text" data-aos-duration="500" data-aos="fade-up"><?php the_sub_field('text'); ?></div>
			</div>
			<?php } 
			if( get_sub_field('image') ) { ?>
			<div class="col-lg-6">
				<div class="image" data-aos-duration="500" data-aos="fade-up">
					<img src="<?php echo get_sub_field('image')['url']; ?>" alt="<?php echo get_sub_field('image')['title']; ?>">
				</div>
			</div>
			<?php } ?>
		</div>
	</div>
</div>