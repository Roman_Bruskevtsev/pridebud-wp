<div class="stage__slider__section">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-md-10">
				<?php 
				$slider = get_sub_field('slider');
				if( $slider ) { ?>
				<div class="stage__slider swiper-container">
					<div class="swiper-wrapper">
						<?php foreach( $slider as $slide ) { ?>
						<div class="swiper-slide">
							<?php if( $slide['title'] ) { ?>
							<div class="title">
								<h6><?php echo $slide['title']; ?></h6>
							</div>
							<?php } ?>
							<div class="content text-center">
								<?php if( $slide['subtitle'] ) { ?>
								<div class="subtitle">
									<h4><?php echo $slide['subtitle']; ?></h4>
								</div>
								<?php } 
								if( $slide['text'] ) { ?>
								<div class="text"><?php echo $slide['text']; ?></div>
								<?php } ?>
							</div>
						</div>
						<?php } ?>
					</div>
					<div class="swiper-button-prev"></div>
  					<div class="swiper-button-next"></div>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
</div>