<div class="services">
    <div class="container">
        <div class="row">
            <div class="col">
                <?php if(get_sub_field('title')) { ?><h2><?php the_sub_field('title'); ?></h2><?php } ?>
                <?php if( have_rows('services') ): ?>
                <div class="services__row">
                    <?php while ( have_rows('services') ) : the_row(); ?>
                    <div class="service__block">
                        <div class="block">
                            <div class="icon">
                                <?php if(get_sub_field('icon')) { ?><img src="<?php the_sub_field('icon'); ?>" alt="<?php the_sub_field('text'); ?>"><?php } ?>
                            </div>
                            <?php if(get_sub_field('text')) { ?><h6><?php the_sub_field('text'); ?></h6><?php } ?>
                        </div>
                    </div>
                    <?php endwhile; ?>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>