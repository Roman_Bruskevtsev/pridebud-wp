<div class="letters__wrapper">
	<div class="container">
		<div class="row">
			<div class="col">
				<?php if(get_sub_field('title')) { ?>
					<h3 data-aos-duration="500" data-aos="fade-up"><?php the_sub_field('title'); ?></h3>
				<?php }
				if( have_rows('images') ): ?>
				<div class="letters__slider" data-aos-duration="500" data-aos="fade-up">
					<?php while ( have_rows('images') ) : the_row(); 
						if( get_sub_field('thumbnail') && get_sub_field('file') ) { ?>
				        <div class="slide">
				            <a href="<?php echo get_sub_field('file'); ?>" target="_blank" class="image">
				            	<img src="<?php echo get_sub_field('thumbnail')['url']; ?>" alt="<?php echo get_sub_field('thumbnail')['title']; ?>">
				            </a>
				        </div>
			        	<?php }
			    	endwhile; ?>
				</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>