<div class="two__columns__section">
	<?php if(get_sub_field('title')) { ?><h2 data-aos-duration="500" data-aos="fade-up"><?php the_sub_field('title'); ?></h2><?php } ?>
	<div class="col__row">
		<?php if( get_sub_field('column_1') ) { ?>
		<div class="col__50">
			<div class="text__column" data-aos-duration="500" data-aos="fade-up"><?php the_sub_field('column_1'); ?></div>
		</div>
		<?php }
		if( get_sub_field('column_2') ) { ?>
		<div class="col__50">
			<div class="text__column" data-aos-duration="500" data-aos="fade-up"><?php the_sub_field('column_2'); ?></div>
		</div>
		<?php } ?>
	</div>
</div>