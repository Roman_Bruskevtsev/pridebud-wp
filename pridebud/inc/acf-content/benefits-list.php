<div class="benefits">
    <div class="container">
        <div class="row">
            <div class="col">
                <?php if(get_sub_field('title')) { ?><h3 data-aos-duration="500" data-aos="fade-up"><?php the_sub_field('title'); ?></h3><?php } ?>
                <?php if( have_rows('benefits') ): ?>
                <ul>
                    <?php while ( have_rows('benefits') ) : the_row(); ?>
                    <li data-aos-duration="500" data-aos="fade-up">
                        <?php if(get_sub_field('icon')) { 
                            $background = ' style="background-image: url('.get_sub_field('icon').');"'; ?>
                            <div class="image"<?php echo $background; ?>></div>
                            <!-- <img src="<?php the_sub_field('icon'); ?>" alt="<?php the_sub_field('text'); ?>"> -->
                        <?php }
                        if(get_sub_field('text')) { ?><h6><?php the_sub_field('text'); ?></h6><?php } ?>
                    </li>
                    <?php endwhile; ?>
                </ul>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>