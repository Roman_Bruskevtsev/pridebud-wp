<div class="three__columns__blocks">
	<div class="container">
		<div class="row">
			<div class="col">
				<?php if( get_sub_field('title') ) { ?>
					<h3 data-aos-duration="500" data-aos="fade-up"><?php the_sub_field('title'); ?></h3>
				<?php } ?>
			</div>
		</div>
		<?php 
		$columns = get_sub_field('columns');
		if( $columns ) { ?>
		<div class="row">
			<?php foreach ( $columns as $column ) { ?>
				<div class="col-md-4">
					<div class="column__block" data-aos-duration="500" data-aos="fade-up">
						<?php if( $column['icon'] ) { ?>
						<div class="icon">
							<img src="<?php echo $column['icon']['url']; ?>" alt="<?php echo $column['icon']['title']; ?>">
						</div>
						<?php } 
						if( $column['title'] ) { ?>
						<h6><?php echo $column['title']; ?></h6>
						<?php } 
						echo $column['text']; ?>
					</div>
				</div>
			<?php } ?>
		</div>
		<?php } ?>
	</div>
</div>