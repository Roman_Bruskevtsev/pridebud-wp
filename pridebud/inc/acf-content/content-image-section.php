<div class="content__image__section">
	<div class="container">
		<div class="row">
			<div class="col-lg-6">
				<?php if( get_sub_field('text') ) { ?>
				<div class="content__block" data-aos-duration="500" data-aos="fade-up">
					<?php the_sub_field('text'); ?>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>
	<?php if( get_sub_field('image') ) { ?>
	<div class="image" style="background-image: url(<?php the_sub_field('image'); ?>);" data-aos-duration="500" data-aos="fade-left"></div>
	<?php } ?>
</div>