<div class="numbers">
    <div class="container">
        <div class="row">
            <div class="col">
                <?php if(get_sub_field('title')) { ?><h3 data-aos-duration="500" data-aos="fade-up"><?php the_sub_field('title'); ?></h3><?php } ?>
                <?php if( have_rows('block') ): ?>
                <div class="numbers__row">
                    <?php 
                    $delay = 200;
                    while ( have_rows('block') ) : the_row(); ?>
                    <div class="number__block" data-aos-duration="500" data-aos="fade-up" data-aos-delay="<?php echo $delay; ?>">
                        <div class="block">
                            <div class="number">
                                <span class="num"><?php the_sub_field('number'); ?></span>
                                <span class="text"><?php the_sub_field('append_text'); ?></span>
                            </div>
                            <div class="desc"><?php the_sub_field('description'); ?></div>
                        </div>
                    </div>
                    <?php 
                    $delay= $delay + 100;
                    endwhile; ?>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>