<div class="benefits__section">
	<?php if( get_sub_field('title') ) { ?>
		<h3 data-aos-duration="500" data-aos="fade-up"><?php the_sub_field('title'); ?></h3>
	<?php } 
	$benefits = get_sub_field('benefits');
	if( $benefits ) { ?>
	<div class="benefits__block">
		<div class="row">
			<?php foreach ( $benefits as $benefit ) { ?>
				<div class="col-md-4">
					<div class="benefit__block" data-aos-duration="500" data-aos="fade-up">
						<?php if( $benefit['icon'] ) { ?>
						<div class="icon">
							<img src="<?php echo $benefit['icon']['url']; ?>" alt="<?php echo $benefit['icon']['title']; ?>">
						</div>
						<?php } 
						if( $benefit['text'] ) { ?>
						<div class="content">
							<?php echo $benefit['text']; ?>
						</div>
						<?php } ?>
					</div>
				</div>
			<?php } ?>
		</div>
	</div>
	<?php } ?>
</div>