<div class="images__gallery">
	<div class="container">
		<?php if( get_sub_field('title') ) { ?>
		<div class="row">
			<div class="col">
				<h3 data-aos-duration="500" data-aos="fade-up"><?php the_sub_field('title'); ?></h3>
			</div>
		</div>
		<?php } 
		$gallery = get_sub_field('images');
		if( $gallery ) { ?>
		<div class="row">
			<div class="col">
				<div class="gallery__slider" data-aos-duration="500" data-aos="fade-up">
					<?php foreach ( $gallery as $slide ) { ?>
						<div class="slide">
							<div class="image" style="background-image: url(<?php echo $slide; ?>);"></div>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>
</div>