<?php
/**
 *
 * @package WordPress
 * @subpackage Pridebud
 * @since 1.0
 * @version 1.0
 */
get_header(); 
$background = (get_field('background_image', 'option')) ? 'style="background-image: url('.get_field('background_image', 'option').');"' : '';
$thumbnail = get_the_post_thumbnail_url(get_the_ID()) ? ' style="background-image: url('.get_the_post_thumbnail_url(get_the_ID(), 'full').');"': '';
?>

        <div class="page__wrapper"<?php echo $background; ?>>
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="content__wrapper content" data-aos="fade-up" data-aos-duration="1000">
                            <div class="service__wrapper">
                                <div class="service__banner text-center"<?php echo $thumbnail; ?>>
                                    <a class="close" href="<?php echo get_post_type_archive_link( 'services' ); ?>">
                                        <span></span>
                                    </a>
                                    <h1><?php the_title(); ?></h1>
                                </div>
                                <div class="service__content">
                                    <?php if( get_field('get_price_button_label', 'option') ) { ?>
                                    <div class="btn__group">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col">
                                                    <button type="button" class="btn yellow__btn"><span><?php the_field('get_price_button_label', 'option'); ?></span></button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php } 
                                    if( have_rows('content') ):
                                        while ( have_rows('content') ) : the_row();
                                            if( get_row_layout() == 'content_editor' ): 
                                                get_template_part( 'inc/acf-content/content-editor' );
                                            elseif( get_row_layout() == 'testimonials_slider' ): 
                                                get_template_part( 'inc/acf-content/testimonials-slider' );
                                            elseif( get_row_layout() == 'form_block' ): 
                                                get_template_part( 'inc/acf-content/form-block' );
                                            elseif( get_row_layout() == 'images_block' ): 
                                                get_template_part( 'inc/acf-content/images-block' );
                                            elseif( get_row_layout() == 'contact_block' ): 
                                                get_template_part( 'inc/acf-content/contact-block' );
                                            elseif( get_row_layout() == 'benefits_block' ): 
                                                get_template_part( 'inc/acf-content/benefits-block' );
                                            elseif( get_row_layout() == 'stage_slider' ): 
                                                get_template_part( 'inc/acf-content/stage-slider' );
                                            elseif( get_row_layout() == 'two_columns_section' ): 
                                                get_template_part( 'inc/acf-content/two-columns-section' );
                                            elseif( get_row_layout() == 'faq__block' ): 
                                                get_template_part( 'inc/acf-content/faq-block' );
                                            endif;
                                        endwhile;
                                    endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


<?php get_footer();