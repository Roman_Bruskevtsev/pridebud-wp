<?php
/**
 *
 * @package WordPress
 * @subpackage Pridebud
 * @since 1.0
 * @version 1.0
 */
get_header(); 
$background = (get_field('background_image', 'option')) ? 'style="background-image: url('.get_field('background_image', 'option').');"' : '';
$width = (get_field('width')) ? ' full' : '';
?>

    <div class="page__wrapper"<?php echo $background; ?>>
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="content__wrapper blog">
                        <?php if( get_field('services_title', 'option') ) { ?><h1><?php the_field('services_title', 'option'); ?></h1><?php } ?>
                        <?php if ( have_posts() ) :
                            echo '<div class="blog__section">';
                            while ( have_posts() ) : the_post();
                                get_template_part( 'template-parts/service/content', 'left' );
                            endwhile;
                            echo '</div>';
                        endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>


<?php get_footer();