<?php
/**
 *
 * @package WordPress
 * @subpackage Pridebud
 * @since 1.0
 * @version 1.0
 */
get_header(); 
$background_option = get_field('background');
$wrapper_class = '';
if( $background_option == '0' || !$background_option ){
    $background = (get_field('background_image', 'option')) ? ' style="background-image: url('.get_field('background_image', 'option').');"' : '';
} else {
    $background = ' style="background-color: #fff;"';
    $wrapper_class = ' dark__content';
}
$width = (get_field('width')) ? ' full' : '';
?>

<div class="page__wrapper"<?php echo $background; ?>>
    <div class="content__wrapper content page<?php echo $width.$wrapper_class; ?>" data-aos="fade-up" data-aos-duration="1000">
        <div class="container">
            <div class="row">
                <div class="col">
                    <div class="page__title">
                        <h1><?php the_title(); ?></h1>
                    </div>
                </div>
            </div>
        </div>
        <?php if( have_rows('content') ):
            while ( have_rows('content') ) : the_row();
                if( get_row_layout() == 'content_editor' ): 
                    get_template_part( 'inc/acf-content/content-editor' );
                elseif( get_row_layout() == 'counter_block' ): 
                    get_template_part( 'inc/acf-content/counter-block' );
                elseif( get_row_layout() == 'testimonials_slider' ): 
                    get_template_part( 'inc/acf-content/testimonials-slider' );
                elseif( get_row_layout() == 'benefits_list' ): 
                    get_template_part( 'inc/acf-content/benefits-list' );
                elseif( get_row_layout() == 'services_block' ): 
                    get_template_part( 'inc/acf-content/services-block' );
                elseif( get_row_layout() == 'form_block' ): 
                    get_template_part( 'inc/acf-content/form-block' );
                elseif( get_row_layout() == 'contact_block' ): 
                    get_template_part( 'inc/acf-content/contact-block' );
                elseif( get_row_layout() == 'letters_slider' ): 
                    get_template_part( 'inc/acf-content/letters-slider' );
                elseif( get_row_layout() == 'three_columns_blocks' ): 
                    get_template_part( 'inc/acf-content/three-columns-blocks' );
                elseif( get_row_layout() == 'content_+_image_section' ): 
                    get_template_part( 'inc/acf-content/content-image-section' );
                elseif( get_row_layout() == 'image_+_content_section' ): 
                    get_template_part( 'inc/acf-content/image-content-section' );
                elseif( get_row_layout() == 'text_image_section_white' ): 
                    get_template_part( 'inc/acf-content/text-image-section-white' );
                elseif( get_row_layout() == 'stage_slider_section' ): 
                    get_template_part( 'inc/acf-content/stage-slider-section' );
                elseif( get_row_layout() == 'prices_section' ): 
                    get_template_part( 'inc/acf-content/prices-section' );
                elseif( get_row_layout() == 'images_gallery' ): 
                    get_template_part( 'inc/acf-content/images-gallery' );
                elseif( get_row_layout() == 'faq_section' ): 
                    get_template_part( 'inc/acf-content/faq-section' );
                elseif( get_row_layout() == 'repair_cost_calculator' ): 
                    get_template_part( 'inc/acf-content/repair-cost-calculator' );
                endif;
            endwhile;
        else :
            echo '
                <section class="padding__section">
                    <div class="container">
                        <div class="row">
                            <div class="col">
                                <div class="page__content">
                                    <div class="no__content">
                                        <h3>'.__('Nothing to show', 'pridebud').'</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            ';
        endif;?>
    </div>
</div>


<?php get_footer();