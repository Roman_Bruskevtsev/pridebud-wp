<?php
/**
 *
 * @package WordPress
 * @subpackage Pridebud
 * @since 1.0
 * @version 1.0
 */
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php wp_head(); ?>
    <?php if( ENV == 'prd' ) { ?>
    <!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window, document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '2699435560269001');
    fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=2699435560269001&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->
    
    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-N7NJHV9');</script>
    <!-- End Google Tag Manager -->
    <meta name="facebook-domain-verification" content="ug20v2qq8svh5obqjsy9t6es2esd4x" />
    <?php } ?>
</head>
<body <?php body_class('load');?>>
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-N7NJHV9"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
    <div class="preloader__wrapper">
        <div class="line"></div>
        <?php if( get_option('preload_text', 'option') && is_front_page() ) { ?>
        <div class="quotes__block">
            <p><?php the_field('preload_text', 'option'); ?></p>
            <small><?php _e('Pridebud', 'pridebud'); ?></small>
        </div>
        <?php } ?>
    </div>
    <div class="mobile__nav">
        <a class="mobile__logo d-block d-xl-none" href="<?php echo esc_url( home_url( '/' ) ); ?>">
            <img src="<?php the_field('logo', 'option'); ?>">
        </a>
        <div class="mobile__btn">
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>
    <header id="header">
        <div class="nav__line">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-xl-7">
                        <?php if( get_field('logo', 'option') ) { ?>
                        <a class="logo float-left" href="<?php echo esc_url( home_url( '/' ) ); ?>">
                            <img src="<?php the_field('logo', 'option'); ?>">
                        </a>
                        <?php } ?>
                        <div class="nav__group float-left">
                            <?php wp_nav_menu( array(
                                'theme_location'        => 'main',
                                'container'             => 'nav',
                                'container_class'       => 'main__navigation float-left'
                            ) ); ?>
                        </div>
                    </div>
                    <div class="col-xl-5">
                        <div class="contacts__info float-right">
                            <?php if( get_field('email', 'option') ) { ?>
                            <div class="email__block float-left">
                                <a href="mailto:<?php the_field('email', 'option'); ?>"><?php the_field('email', 'option'); ?></a>
                            </div>
                            <?php } ?>
                            <?php if( get_field('phones_numbers', 'option') ) { ?>
                            <div class="phone__block<?php echo sizeof( get_field('phones_numbers', 'option') ) == 1 ?' single' : ''; ?> float-left">
                                <a href="tel:<?php echo get_field('phones_numbers', 'option')[0]['phone']; ?>"><?php echo get_field('phones_numbers', 'option')[0]['phone']; ?></a>
                                <?php if( sizeof( get_field('phones_numbers', 'option') ) > 1 ) { ?>
                                <div class="phone__list">
                                    <?php foreach ( get_field('phones_numbers', 'option') as $phone ) { ?>
                                    <a href="tel:<?php echo $phone['phone']; ?>"><?php echo $phone['phone']; ?></a>
                                    <?php } ?>
                                </div>
                                <?php } ?>
                            </div>
                            <?php } ?> 
                            <div class="social__links float-left">
                                <?php if( get_field('facebook', 'option') ) { ?><a href="<?php the_field('facebook', 'option'); ?>" target="_blank" class="facebook"></a><?php } ?> 
                                <?php if( get_field('instagram', 'option') ) { ?><a href="<?php the_field('instagram', 'option'); ?>" target="_blank" class="instagram"></a><?php } ?> 
                            </div>
                            <?php if( get_field('get_price_button_label', 'option') ) { ?>
                            <button type="button" class="btn yellow__btn small float-left"><span><?php the_field('get_price_button_label', 'option'); ?></span></button>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <main>