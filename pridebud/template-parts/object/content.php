<div class="project__block">
    <?php 
    $gallery = get_field('gallery');
    if( $gallery ) { ?>
    <div class="project__slider">
        <?php foreach ( $gallery as $slide ) { 
            $background = ' style="background-image: url('.$slide['sizes']['portfolio-medium'].')"'; ?>
            <div class="slide">
                <div class="image"<?php echo $background; ?>></div>
            </div>
        <?php } ?>
    </div>
    <?php } ?>
    <div class="information__block">
        <div class="content">
            <div class="values__table">
                <div class="values__row">
                    <h6><?php echo TYPEOFWORK; ?></h6>
                    <h5><?php the_title(); ?></h5>
                </div>
                <?php if( get_field('deadline') ) { ?>
                <div class="values__row">
                    <h6><?php echo DEADLINE; ?></h6>
                    <h5><?php the_field('deadline'); ?></h5>
                </div>
                <?php } ?>
                <div class="values__row">
                    <h6><?php echo DESIGN; ?></h6>
                    <h5>
                        <?php if( get_field('design_project') ) { ?>
                            <?php echo YES; ?>
                        <?php } else { ?>
                            <?php echo NO; ?>
                        <?php } ?>
                    </h5>
                </div>
                <?php if( get_field('area') ) { ?>
                <div class="values__row">
                    <h6><?php echo AREA; ?></h6>
                    <h5><?php the_field('area'); ?></h5>
                </div>
                <?php }
                if( get_field('price') ) { ?>
                <div class="values__row">
                    <h6><?php echo PRICE; ?></h6>
                    <h5><?php the_field('price'); ?></h5>
                </div>
                <?php } ?>
            </div>
            <a href="<?php the_permalink(); ?>">
                <span><?php echo SHOWOBJECT; ?></span>
                <i></i>
            </a>
        </div>
    </div>
</div>