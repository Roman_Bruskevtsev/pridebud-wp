<?php 
$background = ( get_the_post_thumbnail( get_the_ID() ) ) ? ' style="background-image: url('.get_the_post_thumbnail_url( get_the_ID(), 'post-small' ).');"' : ''; ?>
<article class="float-left" data-aos="fade-right" data-aos-duration="1000">
    <a href="<?php the_permalink();?>" class="thumbnail"<?php echo $background; ?>></a>
    <div class="content">
        <a href="<?php the_permalink();?>">
            <h2><?php the_title(); ?></h2>
        </a>
        <div class="excerpt">
            <?php the_excerpt(); ?>
        </div>
    </div>
</article>