<?php
/**
 *
 * @package WordPress
 * @subpackage Pridebud
 * @since 1.0
 * @version 1.0
 */
get_header(); 
$background = (get_field('background_image', 'option')) ? 'style="background-image: url('.get_field('background_image', 'option').');"' : '';
while ( have_posts() ) : the_post(); 
    $next_post = get_next_post( false, '', 'objects-categories' );
?>

        <div class="page__wrapper"<?php echo $background; ?>>
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="content__wrapper" data-aos="fade-up" data-aos-duration="1000">
                            <div class="nav__row">
                                <a href="<?php echo get_post_type_archive_link( 'objects' ); ?>">
                                    <i></i>
                                    <span><?php echo BACKTOOBJECTS; ?></span>
                                </a>
                            </div>
                            <div class="project__information">
                                <?php $images = get_field('gallery'); 
                                if($images) { ?>
                                <div class="project__images">
                                    <div class="project__slider">
                                        <?php foreach( $images as $image ): ?>
                                        <div class="slide">
                                            <a href="<?php echo $image['url']; ?>" class="gallery" style="background-image: url(<?php echo $image['sizes']['portfolio-thumbnail']; ?>)">
                                            </a>
                                        </div>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                                <?php } ?>
                                <div class="project__details">
                                    <div class="content">
                                        <h1><?php the_title(); ?></h1>
                                        <div class="table">
                                            <div class="column column__title">
                                                <?php if( get_field('area') ){ ?>
                                                <div class="table__row">
                                                    <span class="icon square"></span>
                                                    <span class="title"><?php echo AREA; ?></span>
                                                </div>
                                                <?php } ?>
                                                <?php if( get_field('deadline') ){ ?>
                                                <div class="table__row">
                                                    <span class="icon term"></span>
                                                    <span class="title"><?php echo DEADLINE; ?></span>
                                                </div>
                                                <?php } ?>
                                                <?php if( get_field('price') ){ ?>
                                                <div class="table__row">
                                                    <span class="icon price"></span>
                                                    <span class="title"><?php echo PRICE; ?></span>
                                                </div>
                                                <?php } ?>
                                            </div>
                                            <div class="column column__value">
                                                <?php if( get_field('area') ){ ?>
                                                <div class="table__row">
                                                    <span class="value"><?php the_field('area'); ?></span>
                                                </div>
                                                <?php } ?>
                                                <?php if( get_field('deadline') ){ ?>
                                                <div class="table__row">
                                                    <span class="value"><?php the_field('deadline'); ?></span>
                                                </div>
                                                <?php } ?>
                                                <?php if( get_field('price') ){ ?>
                                                <div class="table__row">
                                                    <span class="value"><?php the_field('price'); ?></span>
                                                </div>
                                                <?php } ?>
                                            </div>
                                        </div>
                                        <!-- <div class="info">
                                            <?php the_field('description'); ?>
                                        </div> -->
                                        <div class="button__group">
                                            <!-- <button class="btn yellow__btn shadow callback">
                                                <span><?php echo GETCALL; ?></span>
                                            </button> -->
                                            <?php if ($next_post) { ?>
                                            <a href="<?php echo get_permalink( $next_post->ID ); ?>" class="btn white__btn">
                                                <span><?php echo NEXTPROJECT; ?></span>
                                            </a>
                                            <?php } ?>
                                        </div>
                                        <?php if( get_field('sidebar_shortcode', 'option') ) { ?>
                                        <div class="form__block">
                                            <h2><?php echo GETPROJECT; ?></h2>
                                            <?php echo do_shortcode( get_field('sidebar_shortcode', 'option') ); ?>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<?php endwhile;
get_footer();