(function($) {
    $(document).ready(function(){
        $('body').addClass('ready');

        $('.show__services').each(function(){
            $(this).append('<span class="show__bar"></span>');
        });
        $('.service__bar').each(function(){
            $(this).append('<span class="hide__bar"></span>');
        });
        $('.show__bar').on('click', function(){
            $('.service__bar').addClass('show');
            $(this).addClass('hide');
        });
        $('.hide__bar').on('click', function(){
            $('.service__bar').removeClass('show');
            $(this).removeClass('show');
            $('.show__bar').removeClass('hide');
        });
    });
    $(window).on('load', function() {
        $('select').select2({minimumResultsForSearch: -1});
        /*Mobile menu*/
        $('.mobile__btn').on('click', function(){
            $(this).toggleClass('show__menu');
            $('header').toggleClass('show__menu');
            $('.service__bar').removeClass('show');
            $('.show__bar').removeClass('hide');
        })

        /*Popup*/
        $('.callback').on('click', function(){
            $('#get__price').removeClass('show__sidebar');
            $('#get__price').addClass('hide__sidebar');
            $('main').addClass('show__sidebar');
            $('#call__back').removeClass('hide__sidebar');
            $('#call__back').addClass('show__sidebar');
            $('.popup__wrapper').addClass('show__sidebar');
            $('.mobile__btn').removeClass('show__menu');
            $('header').removeClass('show__menu');
        });

        $('header .nav__line button, .service__content .btn__group button').on('click', function(e){
            $('#call__back').removeClass('show__sidebar');
            $('#call__back').addClass('hide__sidebar');
            $('main').addClass('show__sidebar');
            $('#get__price').removeClass('hide__sidebar');
            $('#get__price').addClass('show__sidebar');
            $('.popup__wrapper').addClass('show__sidebar');
            $('.mobile__btn').removeClass('show__menu');
            $('header').removeClass('show__menu');
            e.preventDefault();
        });

        $('.popup__wrapper').on('click', function(){
            $('main').removeClass('show__sidebar');
            $('.popup__sidebar').removeClass('show__sidebar');
            $('.popup__sidebar').addClass('hide__sidebar');
            $('.popup__wrapper').removeClass('show__sidebar');
        });
        /*Preloader*/
        if ($('.preloader__wrapper .quotes__block').length) {
            setTimeout(function(){
                $('.preloader__wrapper').addClass('hide');
                $('body').addClass('load');
            }, 1000);
        } else {
            $('.preloader__wrapper').addClass('hide');
            $('body').addClass('load');
        }

        /*Number counter*/
        // setTimeout(function(){
            $('.number__block .num').each(function () {
                $(this).prop('Counter',0).animate({
                    Counter: $(this).text()
                }, {
                    duration: 2000,
                    easing: 'swing',
                    step: function (now) {
                        $(this).text(Math.ceil(now));
                    }
                });
            });
        // }, 1000);

        /*Mouse Aura effect*/
        // $('body')
        // .mousemove(function(e) {
        //     x  = e.pageX - this.offsetLeft;
        //     y  = e.pageY - this.offsetTop;
        //     xy = x + " " + y;
        //     $('.move__circle').css({
        //         top: y - 23,
        //         left: x - 25
        //     })

        // });
        /*Main slider*/
        if ($('.main__slider').length) {

            $('.main__slider').on('init', function(event, slick){
                $('.slider__navigation .slide').first().addClass('active');
            });
            $('.main__slider').slick({
                infinite: true,
                autoplay: true,
                autoplaySpeed: 10000,
                pauseOnHover: false,
                slidesToShow: 1,
                fade: true,
                speed: 700,
                cssEase: 'linear',
                dots: false,
                arrows: false,
                slidesToScroll: 1,
                responsive:[
                    {
                        breakpoint: 768,
                        settings: {
                            dots: true
                        }
                    }
                ]
            });

            $('.slider__navigation .slide').on('click', function(){
                let slide = $(this).data('slide');
                $(this).addClass('active');
                $('.slider__navigation .slide').not(this).removeClass('active');
                $('.main__slider').slick('slickGoTo', slide);
            });

            $('.main__slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){
                let toNextSlide = nextSlide;
                $('.slider__navigation .slide').each(function(){
                    let slideData = $(this).data('slide');
                    if(slideData === nextSlide){
                        $(this).addClass('active');
                    } else {
                        $(this).removeClass('active');
                    }
                });
            });
        }
        /*Portfolio slider*/
        // if ($('.portfolio__slider').length) {
        //     $('.portfolio__slider').slick({
        //         infinite: true,
        //         vertical: true,
        //         slidesToShow: 3,
        //         dots: false,
        //         arrows: false,
        //         speed: 1000,
        //         slidesToScroll: 1,
        //         verticalSwiping: true,
        //         focusOnSelect: true,
        //         centerMode: true,
        //         centerPadding: 0,
        //         responsive:[
        //             {
        //                 breakpoint: 991,
        //                 settings: {
        //                     slidesToShow: 2
        //                 }
        //             },
        //             {
        //                 breakpoint: 768,
        //                 settings: "unslick"
        //             }
        //         ]
        //     });

        //     let stepUp = 0,
        //         stepDown = 0; 
        //     $('body, html').bind('mousewheel wheel', function(e){
        //         if( e.originalEvent.wheelDelta / 120 > 0 || e.originalEvent.deltaY < 0 ) {
        //             if( stepUp >= 5 ){
        //                 $('.portfolio__slider').slick('slickPrev');
        //                 stepUp = 0;
        //             } else {
        //                 stepUp++;
        //             }
        //         }
        //         else{
        //             if( stepDown >= 5 ){
        //                 $('.portfolio__slider').slick('slickNext');
        //                 stepDown = 0;
        //             } else {
        //                 stepDown++;
        //             }
        //         }
        //     });
        //     $('.portfolio__slider').on('afterChange', function(event, slick, direction){
        //         var currentSlide = parseInt($('.portfolio__slider').slick('slickCurrentSlide')) + 1;
        //         $('.slide__counter .count.current').text(currentSlide);
        //     });
        // }
        if ($('.project__slider').length) {

            $('.project__slider').slick({
                infinite: true,
                slidesToShow: 1,
                dots: true,
                arrows: true,
                speed: 800,
                slidesToScroll: 1,
            });

            $('.project__slider').lightGallery({
                selector: '.gallery',
                download: false,
                counter: false,
            });
        }
        if ($('.testimonials__slider').length) {
            $('.testimonials__slider').slick({
                autoplay: true,
                autoplaySpeed: 10000,
                infinite: true,
                slidesToShow: 1,
                dots: true,
                arrows: false,
                speed: 800,
                slidesToScroll: 1,
            });
        }
        if ($('.letters__slider').length) {
            $('.letters__slider').slick({
                autoplay: true,
                autoplaySpeed: 8000,
                infinite: true,
                slidesToShow: 3,
                dots: true,
                centerMode: true,
                centerPadding: 0,
                arrows: true,
                speed: 800,
                slidesToScroll: 1,
                responsive:[
                    {
                        breakpoint: 992,
                        settings: {
                            arrows: false
                        }
                    },
                    {
                        breakpoint: 768,
                        settings: {
                            arrows: false,
                            slidesToShow: 1
                        }
                    }
                ]
            });
        }
        $('.image__block').lightGallery({
            selector: '.image',
            download: false,
            counter: false,
        });

        if ($('.stage__slider__image').length) {
            let imageSlider = $('.stage__slider__image'),
                navSlider = $('.stage__slider__navigation')
                // sliderNav = $('.stage__slider__navigation .nav__block');
                
            imageSlider.slick({
                autoplay: true,
                autoplaySpeed: 8000,
                infinite: false,
                arrows: true,
                speed: 500,
                slidesToScroll: 1,
                asNavFor: navSlider
            });
            navSlider.slick({
                autoplay: true,
                autoplaySpeed: 8000,
                vertical: true,
                verticalSwiping: true,
                infinite: false,
                arrows: false,
                speed: 500,
                slidesToScroll: 1,
                slidesToShow: 4,
                draggable: false,
                focusOnSelect: true,
                asNavFor: imageSlider
            });

            // imageSlider.on('afterChange', function(event, slick, currentSlide, nextSlide){
            //     slideNav();
            // });

            // sliderNav.each( function(){
            //     $(this).on('click', function(e){
            //         let slideIndex = $(this).index();
            //         imageSlider.slick('slickGoTo', slideIndex);
            //     });
            // });

            // slideNav();

            // function slideNav(){
            //     let currSlide = imageSlider.slick('slickCurrentSlide');
                
            //     sliderNav.each( function(){
            //         $(this).removeClass('active');
            //     });

            //     $(sliderNav[currSlide]).addClass('active');
            // }
        }

        if( $('.faq__wrapper .faq__row') ){
            let faqs = $('.faq__wrapper .faq__row');

            faqs.each( function(){
                $(this).on('click', function(e){
                    $(this).toggleClass('show');
                });
            });
        }

        if ($('.gallery__slider').length) {
            let gallerySlider = $('.gallery__slider');

            gallerySlider.slick({
                infinite: false,
                arrows: true,
                speed: 500,
                rows: 2,
                slidesPerRow: 4,
                slidesToScroll: 1,
                responsive: [
                    {
                        breakpoint: 991,
                        settings: {
                            slidesPerRow: 2
                        }
                    }
                ]
            });
        }

        if($('.price__block').length){
            let priceBlocks = $('.price__block');
            priceBlocks.each( function(){
                let block = $(this),
                    button = block.find('.more__link');
                button.on('click', function(){
                    block.addClass('show');
                });
            });
        }

        if($('.stage__slider').length){
            var swiper = new Swiper('.stage__slider', {
                slidesPerView: 1,
                spaceBetween: 30,
                effect: 'fade',
                autoplay: {
                    delay: 5000,
                    disableOnInteraction: false,
                },
                speed: 500,
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                }
            });
        }

        if($('.company__benefits').length){
            let sections = document.querySelectorAll('.benefit__section'),
                navs = document.querySelectorAll('.benefit__nav__bar h3'),
                sectionTop, sectionHeight, navOrder, sectionOrder;

            navs.forEach( (nav) =>{
                nav.addEventListener('click', () => {
                    navOrder = Array.prototype.slice.call(navs).indexOf(nav);
                    sections[navOrder].scrollIntoView({behavior: 'smooth'});
                });
            });
            

            window.addEventListener('scroll', function() {
                sections.forEach( function(section){
                    sectionTop = parseInt(section.getBoundingClientRect().top);
                    sectionHeight = parseInt(section.getBoundingClientRect().height);
                    
                    if( sectionTop < 120 && Math.abs(sectionTop) < sectionHeight ){
                        sectionOrder = Array.prototype.slice.call(sections).indexOf(section);
                        navs.forEach( (nav) =>{
                            nav.classList.remove('active');
                        });
                        navs[sectionOrder].classList.add('active');
                    }
                });
            });
        }
        
        if($('.repair__cost__calculator__section').length){
            let steps = document.querySelectorAll('.repair__cost__calculator__step'),
                bullets = document.querySelectorAll('.repair__cost__calculator__bullets .bullet'),
                nextSteps = document.querySelectorAll('.steps__navigation .next-step'),
                prevSteps = document.querySelectorAll('.steps__navigation .prev-step'),
                fields = document.querySelectorAll('.repair__cost__calculator__steps .repair__field'),
                formSummary = document.querySelector('.repair__cost__summary'),
                cf7form = document.querySelector('.form__block .wpcf7-form');

            nextSteps.forEach( (btn) => {
                btn.addEventListener('click', () => {
                    let stepsWrapper = Array.prototype.slice.call(steps),
                        step = btn.closest('.repair__cost__calculator__step'),
                        index = stepsWrapper.indexOf(step),
                        field = step.querySelector('input');
                    if( field.checkValidity() ) {
                        field.classList.remove('error');
                        goToNextStep(index + 1);
                    } else {
                        field.classList.add('error');
                    }
                });
            });

            fields.forEach( (field) => {
                field.addEventListener('change', () => {
                    switch(field.name){
                        case 'object-type':
                            formSummary.querySelector('.object__type .value').textContent = field.value;
                            cf7form.querySelector('input[name="object-type"]').value = field.value;
                            break;
                        case 'perimeter-area':
                            formSummary.querySelector('.perimeter__area .value').textContent = field.value;
                            cf7form.querySelector('input[name="perimeter-area"]').value = field.value;
                            break;
                        case 'number-rooms':
                            formSummary.querySelector('.number__rooms .value').textContent = field.value;
                            cf7form.querySelector('input[name="number-rooms"]').value = field.value;
                            break;
                        case 'repair-type':
                            formSummary.querySelector('.repair__type .value').textContent = field.value;
                            cf7form.querySelector('input[name="repair-type"]').value = field.value;
                            break;
                    }
                });
            });

            prevSteps.forEach( (btn) => {
                btn.addEventListener('click', () => {
                    let stepsWrapper = Array.prototype.slice.call(steps),
                        step = btn.closest('.repair__cost__calculator__step'),
                        index = stepsWrapper.indexOf(step);

                    goToNextStep(index - 1);
                });
            });

            // form.addEventListener('submit', (e) => {
            //     e.preventDefault();
            // });

            function goToNextStep(step){
                bullets.forEach( (bullet) => {
                    bullet.classList.remove('active');
                });
                for (let i = 0; i <= step; i++) {
                    bullets[i].classList.add('active');
                }
                steps.forEach( (stepWrapper) => {
                    stepWrapper.classList.remove('active');
                });
                
                steps[step].classList.add('active');
            }
        }

        AOS.init();
    });
})(jQuery);