<?php
/**
 *
 * @package WordPress
 * @subpackage Pridebud
 * @since 1.0
 * @version 1.0
 */
get_header(); 
$background = (get_field('background_image', 'option')) ? 'style="background-image: url('.get_field('background_image', 'option').');"' : '';
$width = (get_field('width')) ? ' full' : '';
?>

        <div class="page__wrapper overflow-x-hidden"<?php echo $background; ?>>
            <div class="content__wrapper content<?php echo $width; ?>" data-aos="fade-left" data-aos-duration="1000">
                <div class="post__title">
                    <div class="container">
                        <div class="row">
                            <div class="col">
                                <h1><?php the_title(); ?></h1>
                            </div>
                        </div>
                    </div>
                </div>
                <?php if( have_rows('content') ):
                    while ( have_rows('content') ) : the_row();
                        if( get_row_layout() == 'content_editor' ): 
                            get_template_part( 'inc/acf-content/content-editor' );
                        elseif( get_row_layout() == 'testimonials_slider' ): 
                            get_template_part( 'inc/acf-content/testimonials-slider' );
                        elseif( get_row_layout() == 'form_block' ): 
                            get_template_part( 'inc/acf-content/form-block' );
                        elseif( get_row_layout() == 'images_block' ): 
                            get_template_part( 'inc/acf-content/images-block' );
                        elseif( get_row_layout() == 'contact_block' ): 
                            get_template_part( 'inc/acf-content/contact-block' );
                        endif;
                    endwhile;
                else :
                    echo '
                        <section class="padding__section">
                            <div class="container">
                                <div class="row">
                                    <div class="col">
                                        <div class="page__content">
                                            <div class="no__content">
                                                <h1>'.__('Nothing to show', 'pridebud').'</h1>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                    ';
                endif;?>
            </div>
        </div>


<?php get_footer();