<?php
/**
 *
 * @package WordPress
 * @subpackage Pridebud
 * @since 1.0
 * @version 1.0
 */
get_header(); 
$background = (get_field('background_image', 'option')) ? 'style="background-image: url('.get_field('background_image', 'option').');"' : '';
?>

    <div class="page__wrapper"<?php echo $background; ?>>
        <div class="content__wrapper" data-aos="fade-up" data-aos-duration="1000">
            <div class="container">
                <div class="row">
                    <div class="col">
                        <div class="page__title">
                            <h1><?php echo PORTFOLIO; ?></h1>
                        </div>
                        <?php if ( have_posts() ) : ?>
                            <div class="portfolio__wrapper">
                            <?php 
                            $i = 0;
                            while ( have_posts() ) : the_post();
                                get_template_part( 'template-parts/object/content');
                            $i++; endwhile; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="information__wrapper">
            <?php 
            $objects = get_field('objects', 'option');
            if( $objects['text'] ) { ?>
            <div class="information__text">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <?php echo $objects['text']; ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php } 
            if( $objects['faq_block'] ) { ?>
            <div class="faq__section">
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <?php if( $objects['faq_block']['title'] ) { ?>
                                <h3 data-aos-duration="500" data-aos="fade-up"><?php echo $objects['faq_block']['title']; ?></h3>
                            <?php } 
                            $faqs = $objects['faq_block']['faq']; 
                            if( $faqs ) { ?>
                            <div class="faq__wrapper">
                                <?php foreach ( $faqs as $faq ) { ?>
                                    <div class="faq__row" data-aos-duration="500" data-aos="fade-up">
                                        <div class="icon"></div>
                                        <?php if( $faq['title'] ) { ?><div class="title"><h6><?php echo $faq['title']; ?></h6></div><?php } ?>
                                        <?php if( $faq['text'] ) { ?><div class="content"><?php echo $faq['text']; ?></div><?php } ?>
                                    </div>
                                <?php } ?>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php } ?>
        </div>
    </div>


<?php get_footer();