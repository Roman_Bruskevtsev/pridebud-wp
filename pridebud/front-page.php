<?php
/**
 *
 * @package WordPress
 * @subpackage Pridebud
 * @since 1.0
 * @version 1.0
 */
get_header(); 
$background = (get_field('background_image', 'option')) ? ' style="background-image: url('.get_field('background_image', 'option').');"' : '';

echo '<div class="page__wrapper"'.$background.'>';

if( have_rows('slider') ): ?>
    <div class="slider__section">
        <div class="main__slider">
            <?php while ( have_rows('slider') ) : the_row(); 
                $background = (get_sub_field('background_image')) ? 'style="background-image: url('.get_sub_field('background_image').');"' : '';
                ?>
                <div class="slide"<?php echo $background; ?>>
                    <div class="content">
                        <?php if( get_sub_field('text') ) { ?>
                        <h2><?php the_sub_field('text'); ?></h2>
                        <?php } ?>
                        <div class="button__group">
                            <?php if( get_sub_field('link_href') ) { ?>
                            <a href="<?php the_sub_field('link_href'); ?>" class="btn yellow__btn shadow">
                                <span><?php the_sub_field('link_label'); ?></span>
                            </a>
                            <?php } ?>
                            <button class="btn white__btn callback">
                                <span><?php echo MASTER; ?></span>
                            </button>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>
        </div>
        <!-- <div class="slider__navigation">
            <?php 
            $i = 0;
            while ( have_rows('slider') ) : the_row(); ?>
            <div class="slide" data-slide="<?php echo $i; ?>">
                <div class="content">
                    <h3><?php echo ($i + 1).' '. get_sub_field('slide_title'); ?></h3>
                    <div class="timeline"></div>
                </div>
            </div>
            <?php $i++; endwhile; ?>
        </div> -->
        <?php if( get_field('large_logo') ) { ?>
        <div class="large__logo">
            <img src="<?php the_field('large_logo'); ?>" alt="<?php _e('PrideBud', 'pridebud'); ?>">
        </div>
    </div>
    <?php } ?>
<?php endif;
if( have_rows('content') ): ?>
    <div class="content__wrapper content">
    <?php while ( have_rows('content') ) : the_row();
        if( get_row_layout() == 'content_editor' ): 
            get_template_part( 'inc/acf-content/content-editor' );
        elseif( get_row_layout() == 'counter_block' ): 
            get_template_part( 'inc/acf-content/counter-block' );
        elseif( get_row_layout() == 'testimonials_slider' ): 
            get_template_part( 'inc/acf-content/testimonials-slider' );
        elseif( get_row_layout() == 'benefits_list' ): 
            get_template_part( 'inc/acf-content/benefits-list' );
        elseif( get_row_layout() == 'services_block' ): 
            get_template_part( 'inc/acf-content/services-block' );
        elseif( get_row_layout() == 'form_block' ): 
            get_template_part( 'inc/acf-content/form-block' );
        elseif( get_row_layout() == 'contact_block' ): 
            get_template_part( 'inc/acf-content/contact-block' );
        elseif( get_row_layout() == 'letters_slider' ): 
            get_template_part( 'inc/acf-content/letters-slider' );
        elseif( get_row_layout() == 'company_benefits_section' ): 
            get_template_part( 'inc/acf-content/company-benefits-section' );
        endif;
    endwhile; ?>
    </div>
<?php endif;

echo '</div>';

get_footer();